package net.tncy.bookmarket.data;

import java.util.ArrayList;

public class Bookstore {

    private int id;
    private String name;
    private ArrayList<InventoryEntry> inventoryEntries;

    public Bookstore(int id, String name, ArrayList<InventoryEntry> inventoryEntries) {
        this.id = id;
        this.name = name;
        this.inventoryEntries = inventoryEntries;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<InventoryEntry> getInventoryEntries() {
        return inventoryEntries;
    }

    public void setInventoryEntries(ArrayList<InventoryEntry> inventoryEntries) {
        this.inventoryEntries = inventoryEntries;
    }


}
