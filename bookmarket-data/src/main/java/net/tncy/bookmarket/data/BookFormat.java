package net.tncy.bookmarket.data;

public enum BookFormat {
    BROCHE, POCHE
}
